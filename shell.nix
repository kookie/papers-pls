with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "gtk-rust-dev";
  buildInputs = with pkgs; [
    rustc cargo rust-analyzer rustfmt

    pkg-config clang_12 clang12Stdenv
    alsa-lib cairo pango atk dbus
    gtk4 glib gdk-pixbuf 
  ];

  ## libappindicator doesn't build unless we point it at libclang.so ??
  LIBCLANG_PATH = "${pkgs.libclang.lib}/lib";
}
