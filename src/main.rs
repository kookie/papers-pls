///////////// GTK4 / RELM4 UI VERSION

// mod data;
// mod message;
// mod model;
// mod view;

// use relm4::RelmApp;

// fn main() {
//     let app = RelmApp::new("de.spacekookie.paperspls");
//     app.run::<model::RootModel>(0);
// }


//
///////////// QT6 / QML UI VERSION


pub mod cxxqt_object;

use cxx_qt_lib::{QGuiApplication, QQmlApplicationEngine, QUrl};

fn main() {
    // Create the application and engine
    let mut app = QGuiApplication::new();
    let mut engine = QQmlApplicationEngine::new();

    // Load the QML path into the engine
    if let Some(engine) = engine.as_mut() {
        engine.load(&QUrl::from("qrc:/qt/qml/com/kdab/cxx_qt/demo/qml/main.qml"));
    }

    // Start the app
    if let Some(app) = app.as_mut() {
        app.exec();
    }
}
