use crate::{
    message::PapersMessage,
    view::{actions::PaperActions, content::ContentArea, RootView},
};
use gtk::glib::clone;
use gtk::prelude::{BoxExt, ButtonExt, GtkWindowExt};
use relm4::gtk::{
    self,
    builders::{ActionBarBuilder, PanedBuilder},
    prelude::*,
    StackSidebar,
};
use relm4::{ComponentParts, ComponentSender, RelmApp, RelmWidgetExt, SimpleComponent};

/// Root application model
pub struct RootModel {
    counter: u8,
}

impl SimpleComponent for RootModel {
    type Input = PapersMessage;
    type Output = ();
    type Init = u8;
    type Root = gtk::Window;
    type Widgets = RootView;

    fn init_root() -> Self::Root {
        gtk::Window::builder()
            .title(crate::data::APP_NAME)
            .default_width(1200)
            .default_height(800)
            .build()
    }

    /// Initialize the UI and model.
    fn init(
        counter: Self::Init,
        window: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> relm4::ComponentParts<Self> {
        let model = RootModel { counter };

        let vbox = gtk::Box::builder()
            .orientation(gtk::Orientation::Vertical)
            .build();

        let actions = PaperActions::new();

        let content = ContentArea::new();
        let content_box = gtk::Box::builder()
            .orientation(gtk::Orientation::Horizontal)
            .build();

        let content_pane = PanedBuilder::new()
            .position(175)
            .shrink_start_child(false)
            .shrink_end_child(false)
            .start_child(&content.sidebar)
            .end_child(&content.stack)
            .build();

        content_box.append(&content_pane);

        vbox.append(&actions.inner);
        vbox.append(&content_box);

        window.set_child(Some(&vbox));
        let widgets = RootView { actions, content };

        ComponentParts { model, widgets }
    }

    fn update(&mut self, msg: Self::Input, _sender: ComponentSender<Self>) {
        match msg {
            PapersMessage::Increment => {
                self.counter = self.counter.wrapping_add(1);
            }
            PapersMessage::Decrement => {
                self.counter = self.counter.wrapping_sub(1);
            }
        }
    }
}
