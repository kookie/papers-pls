use super::list::StackList;
use relm4::gtk::{Stack, StackSidebar};

pub struct ContentArea {
    /// Sidebar area
    pub sidebar: StackSidebar,
    /// Main content area
    pub stack: Stack,
}

impl ContentArea {
    pub fn new() -> Self {
        let stack = Stack::new();

        // TODO: rename this type because it's dumb
        let sample_stack = StackList::sample();
        stack.add_titled(&sample_stack.inner, Some("  SCIENCE  "), "  SCIENCE  ");

        let sidebar = StackSidebar::new();
        sidebar.set_stack(&stack);
        stack.set_vhomogeneous(true);

        Self { stack, sidebar }
    }
}
