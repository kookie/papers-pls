use relm4::gtk::{
    builders::BoxBuilder, prelude::*, Align, Box, Frame, Orientation, PolicyType, ScrolledWindow,
    Viewport,
};

pub struct StackList {
    pub inner: ScrolledWindow,
}

impl StackList {
    pub fn clear(&self) {
        let layout = BoxBuilder::new()
            .orientation(Orientation::Vertical)
            .spacing(8)
            .hexpand(true)
            .vexpand(true)
            .margin_start(32)
            .margin_end(32)
            .halign(Align::Fill)
            .valign(Align::Fill)
            .build();
        
        self.inner.set_child(Some(&layout));
        self.inner.set_hscrollbar_policy(PolicyType::Never);
        self.inner.set_vscrollbar_policy(PolicyType::Always);
    }

    pub fn sample() -> Self {
        let inner = ScrolledWindow::new();
        let this = Self { inner };
        this.clear();

        // ??? Stole this from Irdest and I hope it works
        let inner_child = this.inner.child().unwrap();
        let inner_viewport = inner_child.downcast_ref::<Viewport>().unwrap();
        let viewport_child = inner_viewport.child().unwrap();
        let layout_box = viewport_child.downcast_ref::<Box>().unwrap();

        // Create something to go in the placeholder
        let frame = Frame::new(None);
        frame.set_label(Some("SCIENCE!!!!"));

        // Add the thing to the other thing
        layout_box.append(&frame);

        this
    }
}
