use relm4::gtk::{ActionBar, Button, builders::ActionBarBuilder};

pub struct PaperActions {
    pub inner: ActionBar,
}

impl PaperActions {
    pub fn new() -> Self {
        let add_paper = Button::from_icon_name("list-add");
        let inner = ActionBarBuilder::new().hexpand(true).build();
        inner.pack_start(&add_paper);

        Self { inner }
    }
}
