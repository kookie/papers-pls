pub mod actions;
pub mod content;
pub mod list;

/// A root view container
pub struct RootView {
    pub actions: actions::PaperActions,
    pub content: content::ContentArea,
}
