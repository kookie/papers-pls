# Papers Please

A simple graphical application for managing your research papers
collection.

For the time being this is really more of a "Kookie can have little a Qt6 as a treat" experiment.
If you're interested in building this program, check the 95bd2a246a6016f0983833e828bb3a36d5c4ef89 commit message for details on how to do that, because it's a bit of a mess.